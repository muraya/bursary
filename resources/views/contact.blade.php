<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 2/16/2018
 * Time: 10:33 PM
 */

?>

@extends('layouts.master')
@section('content')

    <!-- breadcumb-area start -->
    <div class="breadcumb-area bg-img-1 black-opacity">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="breadcumb-wrap text-center">
                        <h2>Contact Us</h2>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>/</li>
                            <li>Contact</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->

    <!-- contact-area start -->
    <div class="contact-area ptb-120">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="contact-wrap">
                        <h3>Contact form</h3>
                        <div class="cf-msg"></div>
                        <form action="http://irsfoundation.com/tf/html/syllabus-preview/syllabus/mail.php" method="post" id="cf">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" placeholder="Your Name" id="fname" name="fname">
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <input type="text" placeholder="Your Email" id="email" name="email">
                                </div>
                                <div class="col-xs-12">
                                    <input type="text" placeholder="Subject" id="subject" name="subject">
                                </div>
                                <div class="col-xs-12">
                                    <textarea class="contact-textarea" placeholder="Message" id="msg" name="msg"></textarea>
                                </div>
                                <div class="col-xs-12">
                                    <button id="submit" class="cont-submit btn-contact" name="submit">Send Massage</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact-info">
                        <ul>
                            <li>
                                <span class="flaticon-smartphone"></span>
                                ++88001821216546
                                ++88001245862348
                            </li>
                            <li>
                                <span class="flaticon-opened-email-envelope"></span>
                                mail1@email.com
                                mail2@email.com
                            </li>
                            <li>
                                <span class="flaticon-placeholder"></span>
                                22/1 rokon uddin sorok, mistripara, khulna bangadesh.
                            </li>
                            <li>
                                <span class="flaticon-placeholder"></span>
                                22/1 rokon uddin sorok,mistripara, khulna bangadesh.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="googleMap"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- contact-area end -->

    <script>
        function initialize() {
            var mapOptions = {
                zoom: 15,
                scrollwheel: false,
                center: new google.maps.LatLng(40.712764, -74.005667),
            };

            var map = new google.maps.Map(document.getElementById('googleMap'),
                mapOptions);


            var marker = new google.maps.Marker({
                position: map.getCenter(),
                animation: google.maps.Animation.BOUNCE,
                icon: 'assets/images/map.png',
                map: map
            });

        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

@endsection


