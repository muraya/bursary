<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 13-Jan-18
 * Time: 12:55
 */


?>
@extends('admin.layouts.dashboard')
@section('content')

    <script type='text/javascript'>
        var elementID = document.getElementById('dashboard');
        $(elementID).addClass('active');
    </script>

    <!--page header start-->
    <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
                <h4>Bursaries</h4>
                <ol class="breadcrumb">
                    <li><a href="javascript: void(0);"><i class="fa fa-home"></i></a></li>
                    <li>Admin</li>
                    <li>Bursaries</li>
                    <li> List</li>
                </ol>
            </div>
            <div class="col-sm-6 text-right">
                <div class="title-action">
                    <a href="{{ url('/bursaries/create') }}" class="btn btn-sm btn-primary"> Create Bursary</a>
                </div>
            </div>
        </div>
    </div>
    <!--page header end-->


    <!--start page content-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table id="productsTable" class="table table-striped dt-responsive nowrap table-hover">
                        <thead>
                        <tr>
                            <th style="width:80px">
                                <strong>ID</strong>
                            </th>
                            <th>
                                <strong>NAME</strong>
                            </th>
                            <th>
                                <strong>DESCRIPTION</strong>
                            </th>
                            <th>
                                <strong>CATEGORY</strong>
                            </th>
                            <th>
                                <strong>PRICE</strong>
                            </th>
                            <th style="width:80px">
                                <strong>QUANTITY</strong>
                            </th>
                            <th class="text-center">
                                <strong>STATUS</strong>
                            </th>
                            <th>
                                <strong>ADDED</strong>
                            </th>
                            <th class="text-center">
                                <strong>VIEW</strong>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th style="width:80px">
                                <strong>ID</strong>
                            </th>
                            <th>
                                <strong>NAME</strong>
                            </th>
                            <th>
                                <strong>DESCRIPTION</strong>
                            </th>
                            <th>
                                <strong>CATEGORY</strong>
                            </th>
                            <th>
                                <strong>PRICE</strong>
                            </th>
                            <th style="width:80px">
                                <strong>QUANTITY</strong>
                            </th>
                            <th class="text-center">
                                <strong>STATUS</strong>
                            </th>
                            <th>
                                <strong>ADDED</strong>
                            </th>
                            <th class="text-center">
                                <strong>VIEW</strong>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--end page content-->

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Product";
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');



            var productsTable = $('#productsTable').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Blooming Flowers Product'},
                    {extend: 'pdf', title: 'Blooming Flowers Product'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                "ajax":  "/productsdata",
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'category', name: 'category'},
                    {data: 'price', name: 'price'},
                    {data: 'quantity', name: 'quantity'},
                    {data: 'status', name: 'status'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions'}
                ]
            });


            $(document).on('click', 'a.activate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("activateID").value = id;
                $("#activateTitle").html("Activate " + entity);
                $("#activateNotification").html("Are you sure you want to activate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnActivate').click(function () {

                var id = $("#activateID").val();
                $.ajax({
                    type: "POST",
                    url: "/products/activate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activateModal.modal('hide');
                                    productsTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    productsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.deactivate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deactivateID").value = id;
                $("#deactivateTitle").html("Deactivate " + entity);
                $("#deactivateNotification").html("Are you sure you want to deactivate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDeactivate').click(function () {

                var id = $("#deactivateID").val();
                $.ajax({
                    type: "POST",
                    url: "/products/deactivate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deactivateModal.modal('hide');
                                    productsTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    productsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.isMain', function () {

                var id = $(this).data('id'); // get the item ID
                document.getElementById("deleteID").value = id;

                $.ajax({
                    type: "POST",
                    url: "/products/ismain",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    productsTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.notMain', function () {

                var id = $(this).data('id'); // get the item ID
                document.getElementById("deleteID").value = id;

                $.ajax({
                    type: "POST",
                    url: "/products/notmain",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    productsTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });


            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "/products/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deleteModal.modal('hide');
                                    productsTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });


        });
    </script>

@endsection
