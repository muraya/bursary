<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 11/8/2017
 * Time: 10:21 AM
 */
?>


@extends('admin.layouts.master')
@section('content')

    <script type='text/javascript'>
        var elementID = document.getElementById('categories');
        var users = document.getElementById('main');
        $(elementID).addClass('active');
        $(users).addClass('active');
    </script>

    <!--page header start-->
    <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
                <h4>Products</h4>
                <ol class="breadcrumb">
                    <li><a href="javascript: void(0);"><i class="fa fa-home"></i></a></li>
                    <li>Admin</li>
                    <li>Shop</li>
                    <li> Product</li>
                    <li> Create</li>
                </ol>
            </div>
            <div class="col-sm-6 text-right">
                <div class="title-action">
                </div>
            </div>
        </div>
    </div>
    <!--page header end-->


    <!--start page content-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default collapsed">
                <div class="panel-body">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5> Create Product</h5>
                            </div>
                            <div class="ibox-content">

                                @include('partials.flash-message')

                                <form id="frmCreate" method="post" action="{{'/products'}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <fieldset class="form-horizontal">
                                        <div class="col-md-12">
                                            <label class="control-label" for="name">Name:</label>
                                            <input type="text" class="form-control" name="name" id="name">
                                        </div>
                                        <div class="col-md-12">
                                            <label class="control-label" for="price">Price:</label>
                                            <input type="text" class="form-control" name="price" id="price">
                                        </div>
                                        <div class="col-md-12">
                                            <label class="control-label" for="description">Description:</label>
                                            <textarea class="summernote" name="description" id="description"></textarea>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="category" class="control-label">Category:</label>
                                            <select class="form-control m-b" name="category" id="category">
                                                @foreach($categories as $category)
                                                    <option value="{{ $category -> id }}"> {{ $category -> name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="control-label" for="quantity">Quantity:</label>
                                            <input type="text" class="form-control" name="quantity" id="quantity">
                                        </div>
                                        <div class="col-md-12">
                                            <label class="control-label" for="image"> Primary Image </label>
                                            <input type="file" class="form-control" name="image" id="image" required>
                                        </div>
                                    </fieldset>
                                    <div class="hr-line-dashed"></div>
                                    <fieldset id="submitButton">
                                        <div class="row m-t-lg">
                                            <div class="col-md-8 col-md-offset-2">
                                                <button type="submit" class="btn btn-primary btn-block m"> Save Details <i class="fa fa-save"></i></button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end row-->


    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.summernote').summernote();

        });
    </script>


@endsection


