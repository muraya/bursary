<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 10/27/2017
 * Time: 8:30 AM
 */
?>


@extends('admin.layouts.master')
@section('content')

    <script type='text/javascript'>
        var elementID = document.getElementById('categories');
        var users = document.getElementById('main');
        $(elementID).addClass('active');
        $(users).addClass('active');
    </script>

    <!--page header start-->
    <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
                <h4>Categories</h4>
                <ol class="breadcrumb">
                    <li><a href="javascript: void(0);"><i class="fa fa-home"></i></a></li>
                    <li>Admin</li>
                    <li>Shop</li>
                    <li> Categories</li>
                    <li> Edit </li>
                </ol>
            </div>
            <div class="col-sm-6 text-right">
                <div class="title-action">
                </div>
            </div>
        </div>
    </div>
    <!--page header end-->

    <!--start page content-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default collapsed">
                <div class="panel-body">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5> Create Category</h5>
                            </div>
                            <div class="ibox-content">

                                @include('partials.flash-message')
                                <form role="form" action="{{'/categories/'.$Category->id}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PATCH">
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="name"> Name </label>
                                            <input type="text" name="name" id="name" class="form-control" value="{!! $Category -> name !!}">
                                        </div>
                                        <div class="form-group">
                                            <label for="description"> Description </label>
                                            <textarea name="description" id="description" class="form-control" rows="5">
                                       {!! $Category -> description !!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="main_pic"> Image</label>
                                            <div class="well well-sm">
                                                At least 400px * 550px
                                            </div>
                                            <input id="image" name="image" type="file" accept="file_extension|.png, .jpeg" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="content"> Category Content</label>
                                            <textarea class="summernote" name="content" id="content">{!! $Category -> content !!}</textarea>
                                        </div>
                                    </fieldset>
                                    <fieldset id="submitButton">
                                        <div class="row m-t-lg">
                                            <div class="col-md-8 col-md-offset-2">
                                                <button type="submit" class="btn btn-primary btn-block m"><i class="fa fa-upload"></i> Save Details </button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end row-->


    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.summernote').summernote({height: 200});


        });
    </script>

@endsection
