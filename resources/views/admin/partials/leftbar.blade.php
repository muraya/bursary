<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 1/20/2018
 * Time: 3:56 PM
 */
?>

<!--left navigation start-->
<aside class="float-navigation light-navigation">
    <div class="nano">
        <div class="nano-content">
            <ul class="metisMenu nav" id="menu">
                <li class="nav-heading"><span>Main Navigation</span></li>
                <li><a href="{{ url('admin') }}"><i class="fa fa-diamond"></i> Dashboard </a></li>
                <li class="">
                    <a href="javascript: void(0);" aria-expanded="false">
                        <i class="icon-home"></i> Administrator
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav-second-level nav collapse" aria-expanded="false" style="height: 0px;">
                        <li><a href="{{ url('admin/users') }}"><i class="fa fa-diamond"></i> Users </a></li>
                        <li><a href="{{ url('admin/applicants') }}"><i class="fa fa-diamond"></i> Applicants </a></li>

                    </ul>
                </li>
                <li class="">
                    <a href="javascript: void(0);" aria-expanded="false">
                        <i class="icon-home"></i> Bursaries
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav-second-level nav collapse" aria-expanded="false" style="height: 0px;">
                        <li><a href="{{ url('admin/categories') }}"><i class="fa fa-diamond"></i> Categories </a></li>
                        <li><a href="{{ url('admin/bursaries') }}"><i class="fa fa-diamond"></i> Bursaries </a></li>
                        <li><a href="{{ url('admin/applications') }}"><i class="fa fa-diamond"></i> Applications </a></li>
                        <li><a href="{{ url('admin/disbursements') }}"><i class="fa fa-diamond"></i> Disbursed </a></li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript: void(0);" aria-expanded="false">
                        <i class="icon-home"></i> Options
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav-second-level nav collapse" aria-expanded="false" style="height: 0px;">
                        <li><a href="{{ url('admin/subscriptions') }}"><i class="fa fa-diamond"></i> Subscriptions </a></li>
                        <li><a href="{{ url('admin/contacts') }}"><i class="fa fa-diamond"></i> Contacts </a></li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript: void(0);" aria-expanded="false">
                        <i class="icon-home"></i> Account
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav-second-level nav collapse" aria-expanded="false" style="height: 0px;">
                        <li><a href="{{ url('admin/profile') }}"><i class="fa fa-diamond"></i> My Profile </a></li>
                        <li><a href="{{ url('admin/password') }}"><i class="fa fa-diamond"></i> Change Password </a></li>
                    </ul>
                </li>

            </ul>
        </div><!--nano content-->
    </div><!--nano scroll end-->
</aside>
<!--left navigation end-->
