<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 1/20/2018
 * Time: 3:55 PM
 */
?>

<!--top bar start-->
<div class="top-bar light-top-bar">
    <!--by default top bar is dark, add .light-top-bar class to make it light-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6">
                <a href="{{ url('admin') }}" class="admin-logo">
                    <h1><img src="{{ asset('assets/images/logo.png') }}" alt=""></h1>
                </a>
                <div class="left-nav-toggle visible-xs visible-sm">
                    <a href="#">
                        <i class="glyphicon glyphicon-menu-hamburger"></i>
                    </a>
                </div><!--end nav toggle icon-->
            </div>
            <div class="col-xs-6">
                <ul class="list-inline top-right-nav">
                    <li class="dropdown avtar-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ asset('backend/images/avtar-1.jpg') }}" class="img-circle" width="30" alt="">

                        </a>
                        <ul class="dropdown-menu top-dropdown">
                            <li><a href="{{ url('admin/profile') }}"><i class="icon-user"></i> Profile</a></li>
                            <li><a href="{{ url('admin/password') }}"><i class="icon-key"></i> Password</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
         document.getElementById('logout-form').submit();">
                                    <i class="icon-logout"></i>
                                    Logout
                                </a>
                                <form id="logout-form"
                                      action="{{ url('/logout') }}"
                                      method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>

                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div>
<!-- top bar end-->
