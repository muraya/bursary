<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 13-Jan-18
 * Time: 12:57
 */


?>
@extends('admin.layouts.dashboard')
@section('content')

    <script type='text/javascript'>
        var elementID = document.getElementById('categories');
        var users = document.getElementById('main');
        $(elementID).addClass('active');
        $(users).addClass('active');
    </script>

    <!--page header start-->
    <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
                <h4>Contacts</h4>
            </div>
            <div class="col-sm-6 text-right">
                <ol class="breadcrumb">
                    <li><a href="javascript: void(0);"><i class="fa fa-home"></i></a></li>
                    <li>Admin</li>
                    <li>Options</li>
                    <li> Contacts</li>
                </ol>
            </div>
        </div>
    </div>
    <!--page header end-->

    <!--start page content-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default collapsed">
                <div class="panel-body">
                    <table id="categoriesTable" class="table table-striped table-bordered table-hover display" >
                        <thead>
                        <tr>
                            <th> Name </th>
                            <th> Description </th>
                            <th> Status </th>
                            <th> Last Updated </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th> Name </th>
                            <th> Description </th>
                            <th> Status </th>
                            <th> Last Updated </th>
                            <th> Action </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--end row-->

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Category";
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');



            var categoriesTable = $('#categoriesTable').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Ayopaa Product'},
                    {extend: 'pdf', title: 'Ayopaa Product'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                "ajax":  "/categoriesdata",
                "columns": [
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'status', name: 'status'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions'}
                ]
            });


            $(document).on('click', 'a.activate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("activateID").value = id;
                $("#activateTitle").html("Activate " + entity);
                $("#activateNotification").html("Are you sure you want to activate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnActivate').click(function () {

                var id = $("#activateID").val();
                $.ajax({
                    type: "POST",
                    url: "/categories/activate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activateModal.modal('hide');
                                    categoriesTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    categoriesTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.deactivate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deactivateID").value = id;
                $("#deactivateTitle").html("Deactivate " + entity);
                $("#deactivateNotification").html("Are you sure you want to deactivate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDeactivate').click(function () {

                var id = $("#deactivateID").val();
                $.ajax({
                    type: "POST",
                    url: "/categories/deactivate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deactivateModal.modal('hide');
                                    categoriesTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    categoriesTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.isMain', function () {

                var id = $(this).data('id'); // get the item ID
                document.getElementById("deleteID").value = id;

                $.ajax({
                    type: "POST",
                    url: "/categories/ismain",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    categoriesTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.notMain', function () {

                var id = $(this).data('id'); // get the item ID
                document.getElementById("deleteID").value = id;

                $.ajax({
                    type: "POST",
                    url: "/categories/notmain",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    categoriesTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });


            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "/categories/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deleteModal.modal('hide');
                                    categoriesTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });


        });
    </script>
@endsection