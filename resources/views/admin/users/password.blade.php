<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 6/7/2017
 * Time: 4:15 PM
 */

?>

@extends('admin.layouts.master')
@section('content')

    <script type='text/javascript'>
        var elementID = document.getElementById('mymenu');
        var users = document.getElementById('myprofile');
        $(elementID).addClass('active');
        $(users).addClass('active');
    </script>

    <!--page header start-->
    <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
                <h4>My Password</h4>

            </div>
            <div class="col-sm-6 text-right">
                <ol class="breadcrumb">
                    <li><a href="javascript: void(0);"><i class="fa fa-home"></i></a></li>
                    <li>Account</li>
                    <li> Change Password</li>
                </ol>
            </div>
        </div>
    </div>
    <!--page header end-->

    <!--start page content-->
    <div class="row">
        @include('partials.flash-message')
        <div class="col-lg-8 col-lg-offset-2 ">
            <form class="form-horizontal" action="{{url('/users/password')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label" for="current_password">Current Password</label>
                    <input type="password" id="current_password" name="current_password" class="form-control">
                </div>

                <div class="form-group">
                    <label class="control-label" for="new_password">New Password</label>
                    <input type="password" id="new_password" name="new_password" class="form-control">
                </div>

                <div class="form-group">
                    <label class="control-label" for="confirm_password">Confirm Password</label>
                    <input type="password" id="confirm_password" name="confirm_password" class="form-control">
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-10">
                        <button class="btn btn-primary btn-block" type="submit">Save Password Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div><!--end row-->
    <!--end page content-->

@endsection