<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 3/28/2017
 * Time: 1:18 PM
 */

?>


@extends('admin.layouts.master')
@section('content')

    <script type='text/javascript'>
        var elementID = document.getElementById('mymenu');
        var users = document.getElementById('myprofile');
        $(elementID).addClass('active');
        $(users).addClass('active');
    </script>


    <!--page header start-->
    <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
                <h4>My Profile</h4>
                <ol class="breadcrumb">
                    <li><a href="javascript: void(0);"><i class="fa fa-home"></i></a></li>
                    <li>Account</li>
                    <li> Update Profile</li>
                </ol>
            </div>
            <div class="col-sm-6 text-right">
                <div class="title-action">
                </div>
            </div>
        </div>
    </div>
    <!--page header end-->

    <!--start page content-->
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        @include('partials.flash-message')
                        <form id="frmCreate" method="post" action="{{'/users/profile'}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <section>
                                <div class="form-group">
                                    <label for="name">Full Name</label>
                                    <input type="text" name="name" id="name" class="form-control" value="{!! Auth::user()->name !!}">
                                </div>
                                <div class="form-group">
                                    <label for="sname">Surname</label>
                                    <input type="text" name="sname" id="sname" class="form-control" value="{!! Auth::user()->sname !!}">
                                </div>
                                <div class="form-group">
                                    <label for="email"> Email</label>
                                    <input type="email" name="email" id="email" class="form-control" value="{!! Auth::user()->email !!}">
                                </div>
                                <div class="form-group">
                                    <label for="phone"> Phone</label>
                                    <input type="text" name="phone" id="phone" class="form-control" value="{!! Auth::user()->phone !!}">
                                </div>
                                <div class="form-group">
                                    <label for="avatar"> Image</label>
                                    <input type="file" name="avatar" id="avatar" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button type="submit" id="btnCreate" class="btn btn-primary"> Save Details</button>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
    </div><!--end row-->
    <!--end page content-->

    <!-- Page-Level Scripts -->

@endsection
