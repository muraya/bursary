<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 1/20/2018
 * Time: 4:26 PM
 */

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Common plugins -->
    <link href="{{ asset('backend/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/plugins/simple-line-icons/simple-line-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/plugins/pace/pace.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('backend/plugins/nano-scroll/nanoscroller.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/metisMenu/metisMenu.min.css') }}">
    <!-- dataTables -->
    <link href="{{ asset('backend/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('backend/plugins/summernote/summernote.css') }}" rel="stylesheet">

    <link href="{{ asset('backend/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">

    <link href="{{ asset('backend/plugins/toast/jquery.toast.min.css') }}" rel="stylesheet">

    <!--template css-->
    <link href="{{ asset('backend/css/style.css') }}" rel="stylesheet">


<!--Common plugins-->
    <script src="{{ asset('backend/plugins/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/nano-scroll/jquery.nanoscroller.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/metisMenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('backend/js/float-custom.js') }}"></script>

    <!-- Datatables-->
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables/dataTables.responsive.min.js') }}"></script>

    <script src="{{ asset('backend/plugins/summernote/summernote.min.js') }}"></script>

    <script src="{{ asset('backend/plugins/toast/jquery.toast.min.js') }}"></script>

    <![endif]-->
</head>
<body>

@include('admin.partials.topbar')

{{--@include('admin.partials.rightbar')--}}

@include('admin.partials.leftbar')

<!--main content start-->
<section class="main-content container">

        @yield('content')

        @include('admin.partials.footer')

</section>
<!--end main content-->



</body>

</html>
