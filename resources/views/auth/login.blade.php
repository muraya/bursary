@extends('layouts.app')

@section('content')
<div class="misc-box">
    <p class="text-center text-uppercase pad-v">Login to continue.</p>
    <form role="form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="text-muted" for="email">UserName</label>
            <div class="group-icon">
                <input id="email" name="email" type="email" placeholder="Email" value="{{ old('email') }}" class="form-control" required="">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <span class="icon-user text-muted icon-input"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="text-muted" for="password">Password</label>
            <div class="group-icon">
                <input id="password" name="password" type="password" placeholder="Password" class="form-control">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <span class="icon-lock text-muted icon-input"></span>
            </div>
        </div>
        <div class="clearfix">
            <div class="col-md-12">
                <button type="submit" class="btn btn-block btn-primary">Login</button>
            </div>
        </div>
        <hr>
    </form>
</div>

@endsection
