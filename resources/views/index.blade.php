
    @extends('layouts.index')
    @section('content')

        <!-- slider-area start -->
        <div class="slider-area">
            <div class="slider-active">
                <div class="slider-items">
                    <img src="{{ asset('assets/images/slider/1.jpg') }}" alt="" class="slider">
                    <div class="slider-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-1">
                                    <h2>Bursary Management System</h2>
                                    <p> One stop platform for application, verifying and approval of bursaries in our constituency.</p>
                                    <a href="#">View Available Bursaries </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slider-items">
                    <img src="{{ asset('assets/images/slider/2.jpg') }}" alt="" class="slider">
                </div>
                <div class="slider-items">
                    <img src="{{ asset('assets/images/slider/3.jpg') }}" alt="" class="slider">
                    <div class="slider-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-1">
                                    <h2> Online Platform </h2>
                                    <p> Offers seamless and transparent application process that  can ensure we get the available funds evenly.</p>
                                    <a href="#">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slider-items">
                    <img src="{{ asset('assets/images/slider/4.jpg') }}" alt="" class="slider">
                </div>
            </div>
        </div>
        <!-- slider-area end -->

        <!-- featured-area start -->
        <div class="featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 fix">
                        <div class="featured-wrap">
                            <i class="flaticon-trophy"></i>
                            <h2>Award winning</h2>
                            <p>Our Platform is the most finest of systems in our country as it offers seamless application of bursaries.</p>
                        </div>
                        <div class="featured-wrap">
                            <i class="flaticon-study"></i>
                            <h2>Experienced Panel </h2>
                            <p> The panel that reviews and approves the application are there to ensure equal distribution of the available funds.</p>
                        </div>
                        <div class="featured-wrap">
                            <i class="flaticon-graduate-student-avatar"></i>
                            <h2>Security</h2>
                            <p> Your information is safe with us. For transparency we only publish those who have won the bursaries on this platform.</p>
                        </div>
                        <div class="featured-wrap">
                            <i class="flaticon-open-book"></i>
                            <h2> Information and Updates </h2>
                            <p> The platform offers information concerning new bursary application sessions and also information on disbursements.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- featured-area end -->

        <!-- service-area start -->
        <div class="service-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-title text-center">
                            <h2>The Application Process</h2>
                            <p> This outlines the whole process to submit your bursary application.</p>
                            <ul class="section-line">
                                <li class="first"></li>
                                <li class="second"></li>
                                <li class="third"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 line-service col-sm-6 col col-xs-12">
                        <div class="service-wrap text-center">
                            <div class="service-icon">
                                <span class="flaticon-draft"></span>
                            </div>
                            <h3>Make registration</h3>
                            <p>Registration is the first process and only applicable to applicants within our constituency .</p>
                        </div>
                    </div>
                    <div class="col-md-3 line-service col-sm-6 col col-xs-12">
                        <div class="service-wrap text-center">
                            <div class="service-icon">
                                <span class="flaticon-classroom"></span>
                            </div>
                            <h3>Select your Bursary</h3>
                            <p> Here you select the bursary scheme and fill the applicable amount.</p>
                        </div>
                    </div>
                    <div class="col-md-3 line-service col-sm-6 col col-xs-12">
                        <div class="service-wrap text-center">
                            <div class="service-icon">
                                <span class="flaticon-clipboard-with-pencil"></span>
                            </div>
                            <h3> Document Upload</h3>
                            <p> Here you upload signed and stamped document from your area government representative.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col col-xs-12">
                        <div class="service-wrap text-center">
                            <div class="service-icon">
                                <span class="flaticon-person-learning-by-reading"></span>
                            </div>
                            <h3> Get the Bursary</h3>
                            <p> We will process the application and keep you up to date with the application process.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- service-area end -->

        <!-- free-account-area start -->
        <div class="free-account-area black-opacity  parallax black-opacity" data-speed="3" data-bg-image="assets/images/bg/1.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-7 col-xs-12">
                        <div class="free-account-info">
                            <h2>Start learning form today</h2>
                            <h3>10% - 15% off for next 15 days</h3>
                            <p>Learing is not easy every time optmize game thing will not maintain anatome synomems certificates get thing easy, everybody love annual experience so mein gaming coverstion.</p>
                            <a href="#">sell all courses</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-12">
                        <div class="free-account-form">
                            <h2>Create Free Account</h2>
                            <form action="#">
                                <input type="text" placeholder="Your Name">
                                <input type="email" placeholder="Your Email">
                                <input type="text" placeholder="Your Phone Number">
                                <button type="button" name="button">submit now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- free-account-area end -->

        <!-- .testmonial-area start -->
        <div class="testmonial-area ptb-120">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-title text-center">
                            <h2>success story</h2>
                            <p>Some of our students quote aobut ther learning time and success.</p>
                            <ul class="section-line">
                                <li class="first"></li>
                                <li class="second"></li>
                                <li class="third"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 hidden-sm">
                        <div class="test-wrap">
                            <div class="video-wrap black-opacity">
                                <img src="assets/images/testmonial.jpg" alt="">
                                {{--<a href="https://www.youtube.com/watch?v=SrURbW1Kx14" class="video-popup"><span class="flaticon-play-button"></span></a>--}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="testmonial-wrapper">
                            <div class="test-active">
                                <div class="test-items">
                                    <div class="test-img">
                                        <img src="assets/images/test/1.jpg" alt="">
                                    </div>
                                    <div class="test-info">
                                        <p>“ Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo quat.” </p>
                                        <h3><span>Kasidpal Rang</span> - CEO omok Instute</h3>
                                    </div>
                                </div>
                                <div class="test-items">
                                    <div class="test-img">
                                        <img src="assets/images/test/2.jpg" alt="">
                                    </div>
                                    <div class="test-info">
                                        <p>“ Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo quat.” </p>
                                        <h3><span>Kasidpal Rang</span> - CEO omok Instute</h3>
                                    </div>
                                </div>
                                <div class="test-items">
                                    <div class="test-img">
                                        <img src="assets/images/test/1.jpg" alt="">
                                    </div>
                                    <div class="test-info">
                                        <p>“ Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo quat.” </p>
                                        <h3><span>Kasidpal Rang</span> - CEO omok Instute</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .testmonial-area end -->

        <!-- fanfact-area start -->
        <div class="fanfact-area parallax black-opacity ptb-120" data-speed="3" data-bg-image="assets/images/bg/2.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-title text-center white-section-title">
                            <h2>IMPORTANT FACTS</h2>
                            <p>we are the most experience and dedecited instute in this business.</p>
                            <ul class="section-line">
                                <li class="first"></li>
                                <li class="second"></li>
                                <li class="third"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <div class="funfact-wrap">
                            <h3 ><span class="counter">500</span> +</h3>
                            <p>Students</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="funfact-wrap">
                            <h3 ><span class="counter">50</span> +</h3>
                            <p>Teachers</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="funfact-wrap">
                            <h3 ><span class="counter">70</span> +</h3>
                            <p>Courses</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="funfact-wrap">
                            <h3 ><span class="counter">20</span> +</h3>
                            <p>Countries</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- fanfact-area end -->



        @endsection
