<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 2/16/2018
 * Time: 10:37 PM
 */
?>


@extends('layouts.master')
@section('content')

    <!-- breadcumb-area start -->
    <div class="breadcumb-area bg-img-1 black-opacity">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="breadcumb-wrap text-center">
                        <h2>alumni stories</h2>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>/</li>
                            <li>alumni stories</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->

    <!-- .alumni-area start -->
    <div class="alumni-area ptb-120">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="alumni-sidebar">
                        <div class="alumni-menu mb-50">
                            <ul>
                                <li class="active"><a href="#">Michel Amder Jabuju</a></li>
                                <li><a href="#">Rini Simir Bon</a></li>
                                <li><a href="#">Danial Deplan Jokes</a></li>
                                <li><a href="#">David Notun Patare</a></li>
                                <li><a href="#">Seusie Nmaspi</a></li>
                                <li><a href="#">Seusie Nmaspi</a></li>
                            </ul>
                        </div>
                        <div class="alumni-brand mb-50">
                            <div class="brand-img">
                                <img src="assets/images/alumni/2.jpg" alt="">
                            </div>
                            <h3>Extra curricular activities</h3>
                        </div>
                        <div class="alumni-brand mb-50">
                            <div class="brand-img">
                                <img src="assets/images/alumni/3.jpg" alt="">
                            </div>
                            <h3>Computer Science & IT</h3>
                        </div>
                        <div class="alumni-brand mb-50">
                            <div class="brand-img">
                                <img src="assets/images/alumni/4.jpg" alt="">
                            </div>
                            <h3>Annual Sports</h3>
                        </div>
                    </aside>
                </div>
                <div class="col-md-9">
                    <div class="alumni-wrap">
                        <div class="alumni-img">
                            <img src="assets/images/alumni/1.jpg" alt="">
                        </div>
                        <div class="alumni-text">
                            <blockquote>“As a old student, i would like to say my college depending anything not easy early event saving, nothing mean so do the compouter syllabus mean go ride. I would mean appoinment so nothing.”<span> -- Michel Amder Jabuju</span> </blockquote>
                        </div>
                        <div class="alumni-content">
                            <p>Nothing is not defficult consequuntur cum, summo invidunt ne eum. Ea percipit oporteat vim, mel eius inermis intellegebat an, vix in singulis evertitur intellegam. Duo an falli equidem, pri id nihil tritani repudiare. Ius tempor causae albucius in, vitae adolescens cu est, ut modus animal iracundia quo. Per vocent scribentur eu, has zril sensibus an. Probo molestie vel cu, no sint causae eirmod nam. oportere. Ut per tantas commodo nostrum, et assum maluisset mediocritatem duo, ne est oratio assentior. Ei ius tale dissentiunt, ancillae scaevola id pri, vim velit virtute efficiendi et. In mei omnes fierent. Id fuisset ponderum mei. At pri appetere conceptam eloquentiam.</p>
                            <p>Go aheade to learn your gole  Rebum malis cum te, eripuit instructior comprehensam sea ei. Ea deleniti verterem eleifend eam, dico torquatos et has. At mea sint forensibus repudiandae. In his vituperata reprehendunt, quis senserit qui ex, usu ei partemaltera. Vis et duis ludus fabellas, in vis graeci reprimique. Deserunt dissentias ius eu, eu vis menandri definitionem. Cumliber gubergren ei, eam in everti omnium noluisse, choro euripidis efficiendi nec ne.</p>
                            <p>Oppertunity come very ofen congue prompta. Esse nostro delenit vix te. Mel eu choro sadipscing adversarium. Ea usu suas sanctus nonumes, sit ornatus convenire interpretaris ei. Et qui utinam audiam. Esse civibus copiosae ne mea. No mei semper tibique indoctum, nam liber civibus maluisset . Aterum ornatus ea, aeterno consectetuer concludaturque mei ex. Ad nec commodo incorrupte, id pri admodum tractatos, at eam tempor legendos. Has an quis ferri, ius cu wisi principes scriptorem. Nec praesent laboramus deseruisse ad, nec ea quidam mentitum repudiare. Cum at mutat illud, mei vide graece detraxit ex, cu has admodum eleifend. Eu cum labitur tacimates, duis utroque ceteros nam in. Case convenire referrentur id pri.</p>
                            <img src="assets/images/sign.png" alt="">
                            <ul>
                                <li>Share this story: </li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .alumni-area end -->


@endsection


