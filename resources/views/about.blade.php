<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 2/16/2018
 * Time: 10:28 PM
 */
?>

@extends('layouts.master')
@section('content')

    <!-- breadcumb-area start -->
    <div class="breadcumb-area bg-img-1 black-opacity">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="breadcumb-wrap text-center">
                        <h2>About us</h2>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>/</li>
                            <li>about</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->

    <!-- .about-style-area start -->
    <div class="about-style-area ptb-120">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="about-images black-opacity">
                        <img src="assets/images/about/about.jpg" alt="">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="about-style-wrap">
                        <h3>Welcome to syallabus</h3>
                        <p>Welcome to our instute Mazim volutpat per in, quo in audire mandamus tincidunt, cibo autem eu sed. Pri id tantas epicurei invenire, <strong>modus eloquentiam est ex.</strong> In elit aperiri mea. Pri debet disputationi ut, mei in meis rationibus.</p>
                        <p>We can ensure that no other laudem ornatus offendit eam, et ius adhuc honestatis, lucilius recusabo at vis. In pro quis indoctum, ex minim interesset quo, sed unum quidam cu. Alii everti eu mel. Dicant malorum dissentiunt at ius. Te agam quot nec, eos albucius quaerendum id. Mei ea ferri fierent, vero nullam usu te, cu qui quod graeco melius.ex minim interesset quo, sed unum quidam cu. Alii everti eu mel. Dicant malorum dissentiunt at ius. Te agam quot nec, eos </p>
                        <p><span >“ Our Princiapl every time quidam inciderint, ex eum nibh soluta. Mel atomorum partiendo maluisset no. Sed noluisse dissentias philosophia cu, ut est lorem nobis inermis. Ne detraxit intellegam nam. In nostro pericula repudiandae eum, usu et accumsan disputando placerat.”</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .about-style-area end -->

    <!-- .service-our-company start -->
    <div class="service-our-company">
        <div class="company-service-img">
            <img src="assets/images/service/service.jpg" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="service-items">
                                <div class="company-service-icon">
                                    <span class="flaticon-male-cartoon-pointing-to-white-board"></span>
                                </div>
                                <div class="service-info">
                                    <h2>Certifide Teachers</h2>
                                    <p>A certified teacher is a teacher who has earned credentials from an authoritative source, such as the govern</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="service-items">
                                <div class="company-service-icon">
                                    <span class="flaticon-microscope-side-view"></span>
                                </div>
                                <div class="service-info">
                                    <h2>Library Services</h2>
                                    <p>A certified teacher is a teacher who has earned credentials from an authoritative source, such as the govern</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="service-items">
                                <div class="company-service-icon">
                                    <span class="flaticon-chemistry-flask-with-liquid-inside"></span>
                                </div>
                                <div class="service-info">
                                    <h2>Practical Lab</h2>
                                    <p>A certified teacher is a teacher who has earned credentials from an authoritative source, such as the govern</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="service-items">
                                <div class="company-service-icon">
                                    <span class="flaticon-diploma"></span>
                                </div>
                                <div class="service-info">
                                    <h2>Certification</h2>
                                    <p>A certified teacher is a teacher who has earned credentials from an authoritative source, such as the govern</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="service-items">
                                <div class="company-service-icon">
                                    <span class="flaticon-bus-side-view"></span>
                                </div>
                                <div class="service-info">
                                    <h2>Bus services</h2>
                                    <p>A certified teacher is a teacher who has earned credentials from an authoritative source, such as the govern</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="service-items">
                                <div class="company-service-icon">
                                    <span class="flaticon-meeting"></span>
                                </div>
                                <div class="service-info">
                                    <h2>Hostel services</h2>
                                    <p>A certified teacher is a teacher who has earned credentials from an authoritative source, such as the govern</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .service-our-company end -->

    <div class="quote-area2">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <div class="quote-wrap2">
                        <p>World class courses form our institute. Don’t hesitate to contact us for details</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="quote-wrap2 text-right">
                        <ul>
                            <li><a href="contact.html">Contact Us</a></li>
                            <li><a href="apply-online.html">Apply Now</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

