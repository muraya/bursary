<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 2/16/2018
 * Time: 10:08 PM
 */

?>

<!-- footer -->
<footer class="footer-area">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="footer-widget contact">
                        <h2>CONTACT</h2>
                        <ul>
                            <li><i class="fa fa-location-arrow"></i> 22/1 Rokon uddin sokon uddin Sorok, Mistripara, Khulna, Bangladesh.</li>
                            <li><i class="fa fa-envelope-o"></i> email@email.com</li>
                            <li><i class="fa fa-phone"></i> ++8800412154</li>
                            <li><i class="fa fa-fax"></i> 85468454578</li>
                            <li><i class="fa fa-clock-o"></i> Mon - Sat: 10AM - 6PM</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="footer-widget footer-menu">
                        <h2>Resources</h2>
                        <ul>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Carrers</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Job list</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Academic Seminar</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Teachers Lists</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Learing Opportunity</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Annual Sports</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="footer-widget recent-post">
                        <h2>Recent posts</h2>
                        <ul>
                            <li>
                                <div class="post-img">
                                    <a href="#"><img src="assets/images/post/1.jpg" alt="" class="img img-responsive"></a>
                                </div>
                                <div class="post-ceontent">
                                    <h3><a href="#">Our annual sports will be held on 26 dec 2017.</a></h3>
                                    <span><i class="fa fa-calendar"></i> 19 Otc, 2017</span>
                                </div>
                            </li>
                            <li>
                                <div class="post-img">
                                    <a href="#"><img src="assets/images/post/2.jpg" alt="" class="img img-responsive"></a>
                                </div>
                                <div class="post-ceontent">
                                    <h3><a href="#">Our annual sports will be held on 26 dec 2017.</a></h3>
                                    <span><i class="fa fa-calendar"></i> 19 Otc, 2017</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-bottom fix">
                        <p class="pull-left">© Copyright 2018</p>
                        <ul class="pull-right">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer -->
