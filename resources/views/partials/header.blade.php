<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 2/16/2018
 * Time: 10:08 PM
 */

?>


<!-- header-area start -->
<header class="header-area">
    <div class="header-top bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-8 col-xs-12">
                    <div class="header-top-left">
                        <ul>
                            <li>For your question</li>
                            <li><i class="fa fa-phone"></i>+88014515642</li>
                            <li><i class="fa fa-envelope-o"></i>example@ex.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-4 col-xs-12">
                    <div class="header-top-right text-right">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-10 col-xs-10">
                    <div class="logo">
                        <a href="{{ url('') }}">
                            <img src="{{ asset('assets/images/logo.png') }}" alt="" />
                            <span class="hidden">Logo</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-8 hidden-sm hidden-xs">
                    <div class="mainmenu text-right">
                        <ul id="navigation">
                            <li class="active"><a href="{{ url('') }}">Home</a></li>
                            <li><a href="{{ url('about') }}">About</a></li>
                            <li><a href="{{ url('bursary') }}"> Bursaries </a></li>
                            <li><a href="{{ url('organisation') }}"> Organisations  </a></li>
                            <li><a href="{{ url('contact') }}">Contacts </a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2 clear col-xs-2 hidden-md hidden-lg">
                    <div class="responsive-menu-wrap floatright"></div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-area end -->
