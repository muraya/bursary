


<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Common plugins -->
    <link href="backend/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="backend/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet">
    <link href="backend/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="backend/plugins/pace/pace.css" rel="stylesheet">
    <link href="backend/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="backend/plugins/nano-scroll/nanoscroller.css">
    <link rel="stylesheet" href="backend/plugins/metisMenu/metisMenu.min.css">
    <!--for checkbox-->
    <link href="backend/plugins/iCheck/blue.css" rel="stylesheet">
    <!--template css-->
    <link href="backend/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        html,body{
            height: 100%;
        }
    </style>

    <!--Common plugins-->
    <script src="backend/plugins/jquery/dist/jquery.min.js"></script>
    <script src="backend/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="backend/plugins/pace/pace.min.js"></script>
    <script src="backend/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
    <script src="backend/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="backend/plugins/nano-scroll/jquery.nanoscroller.min.js"></script>
    <script src="backend/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="backend/js/float-custom.js"></script>
    <!--ichecks-->
    <script src="plugins/iCheck/icheck.min.js"></script>


</head>
<body>

<div class="misc-wrapper">
    <div class="misc-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                    <div class="misc-header text-center">
                        <img src="{{ asset('assets/images/logo.png') }}" alt="">
                    </div>

                    @yield('content')

                    <div class="text-center misc-footer">
                        <span>&copy; Copyright 2016 - 2017. Float Admin<br>Bootstrap admin template</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
    });
</script>
</body>

</html>

