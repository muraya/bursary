<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 2/16/2018
 * Time: 10:16 PM
 */
?>


<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bursary Management System - Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/images/favicon.png') }}">
    <!-- Place favicon.ico in the root directory -->

    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <!-- owl.carousel.2.0.0-beta.2.4 css -->
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}">
    <!-- font-awesome v4.6.3 css -->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <!-- magnific-popup.css -->
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
    <!-- slicknav.min.css -->
    <link rel="stylesheet" href="{{ asset('assets/css/slicknav.min.css') }}">
    <!-- flaticon.css -->
    <link rel="stylesheet" href="{{ asset('assets/css/flaticon.css') }}">
    <!-- slick-theme.css -->
    <link rel="stylesheet" href="{{ asset('assets/css/slick-theme.css') }}">
    <!-- slick.css -->
    <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <!-- modernizr css -->
    <script src="{{ asset('assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- preloder-wrap -->
<div id="cssLoader3" class="preloder-wrap">
    <div class="loader">
        <div class="child-common child4"></div>
        <div class="child-common child3"></div>
        <div class="child-common child2"></div>
        <div class="child-common child1"></div>
    </div>
</div>
<!-- preloder-wrap -->

@include('partials.header')

@yield('content')

@include('partials.footer')

<!-- all js here -->
<!-- jquery latest version -->
<script src="{{ asset('assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
<!-- bootstrap js -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<!-- owl.carousel.2.0.0-beta.2.4 css -->
<script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<!-- counterup.main.js -->
<script src="{{ asset('assets/js/counterup.main.js') }}"></script>
<!-- isotope.pkgd.min.js -->
<script src="{{ asset('assets/js/imagesloaded.pkgd.min.js') }}"></script>
<!-- isotope.pkgd.min.js -->
<script src="{{ asset('assets/js/isotope.pkgd.min.js') }}"></script>
<!-- jquery.waypoints.min.js -->
<script src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>
<!-- jquery.slicknav.min.js -->
<script src="{{ asset('assets/js/jquery.slicknav.min.js') }}"></script>
<!-- jquery.magnific-popup.min.js -->
<script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
<!-- wow js -->
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<!-- slick.min.js -->
<script src="{{ asset('assets/js/slick.min.js') }}"></script>
<!-- plugins js -->
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<!-- main js -->
<script src="{{ asset('assets/js/scripts.js') }}"></script>
</body>

</html>
