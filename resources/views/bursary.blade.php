<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 2/16/2018
 * Time: 10:32 PM
 */

?>

@extends('layouts.master')
@section('content')

    <!-- breadcumb-area start -->
    <div class="breadcumb-area bg-img-1 black-opacity">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="breadcumb-wrap text-center">
                        <h2> Bursaries </h2>
                        <ul>
                            <li><a href="{{ url('') }}">Home</a></li>
                            <li>/</li>
                            <li>Course</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->

    <!-- popular-courses-area start -->
    <div class="popular-courses-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="popular-courses-wrap">
                        <div class="popular-courses-img">
                            <img src="assets/images/service/1.jpg" alt="">
                        </div>
                        <div class="popular-courses-content">
                            <div class="author-review fix">
                                <ul class="pull-left">
                                    <li>By: <a href="#">Jhon dow</a></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><span>Review: </span></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-o"></i></li>
                                </ul>
                            </div>
                            <div class="popular-course-info">
                                <h3><a href="#">laboratory - mandatory laboratory</a></h3>
                                <p>A medical laboratory or clinical laboratory is a boratorys pertaining to the diagnosis, treatment, and prevention of disease. information about the  game is not over.</p>
                            </div>
                            <div class="course-footer fix">
                                <ul class="pull-left">
                                    <li><span>Courses: <strong>1 year</strong></span></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><a href="#"><i class="fa fa-thumbs-o-up"></i></a></li>
                                    <li><a href="#"><i class="fa fa-commenting-o"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="popular-courses-wrap">
                        <div class="popular-courses-img">
                            <img src="assets/images/service/2.jpg" alt="">
                        </div>
                        <div class="popular-courses-content">
                            <div class="author-review fix">
                                <ul class="pull-left">
                                    <li>By: <a href="#">Jhon dow</a></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><span>Review: </span></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                            <div class="popular-course-info">
                                <h3><a href="#">physics - physics game for learners</a></h3>
                                <p>branch of science concerned with the nature and properties of matter and energy. The subject matter of physics, distinguished from that of chemistry and biology matter.</p>
                            </div>
                            <div class="course-footer fix">
                                <ul class="pull-left">
                                    <li><span>Courses: <strong>1 year</strong></span></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><a href="#"><i class="fa fa-thumbs-o-up"></i></a></li>
                                    <li><a href="#"><i class="fa fa-commenting-o"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="popular-courses-wrap">
                        <div class="popular-courses-img">
                            <img src="assets/images/service/3.jpg" alt="">
                        </div>
                        <div class="popular-courses-content">
                            <div class="author-review fix">
                                <ul class="pull-left">
                                    <li>By: <a href="#">Jhon dow</a></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><span>Review: </span></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-o"></i></li>
                                </ul>
                            </div>
                            <div class="popular-course-info">
                                <h3><a href="#">Code Create - Mandatory</a></h3>
                                <p>The branch of science concerned with the nature and properties of matter and energy. The subject matter of physics, distinguished from that of chemistry and biology.</p>
                            </div>
                            <div class="course-footer fix">
                                <ul class="pull-left">
                                    <li><span>Courses: <strong>1 year</strong></span></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><a href="#"><i class="fa fa-thumbs-o-up"></i></a></li>
                                    <li><a href="#"><i class="fa fa-commenting-o"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="popular-courses-wrap">
                        <div class="popular-courses-img">
                            <img src="assets/images/service/4.jpg" alt="">
                        </div>
                        <div class="popular-courses-content">
                            <div class="author-review fix">
                                <ul class="pull-left">
                                    <li>By: <a href="#">Jhon dow</a></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><span>Review: </span></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-o"></i></li>
                                </ul>
                            </div>
                            <div class="popular-course-info">
                                <h3><a href="#">laboratory - mandatory laboratory</a></h3>
                                <p>A medical laboratory or clinical laboratory is a boratorys pertaining to the diagnosis, treatment, and prevention of disease. information about the  game is not over.</p>
                            </div>
                            <div class="course-footer fix">
                                <ul class="pull-left">
                                    <li><span>Courses: <strong>1 year</strong></span></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><a href="#"><i class="fa fa-thumbs-o-up"></i></a></li>
                                    <li><a href="#"><i class="fa fa-commenting-o"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="popular-courses-wrap">
                        <div class="popular-courses-img">
                            <img src="assets/images/service/8.jpg" alt="">
                        </div>
                        <div class="popular-courses-content">
                            <div class="author-review fix">
                                <ul class="pull-left">
                                    <li>By: <a href="#">Jhon dow</a></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><span>Review: </span></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                            <div class="popular-course-info">
                                <h3><a href="#">physics - physics game for learners</a></h3>
                                <p>branch of science concerned with the nature and properties of matter and energy. The subject matter of physics, distinguished from that of chemistry and biology matter.</p>
                            </div>
                            <div class="course-footer fix">
                                <ul class="pull-left">
                                    <li><span>Courses: <strong>1 year</strong></span></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><a href="#"><i class="fa fa-thumbs-o-up"></i></a></li>
                                    <li><a href="#"><i class="fa fa-commenting-o"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="popular-courses-wrap">
                        <div class="popular-courses-img">
                            <img src="assets/images/service/6.jpg" alt="">
                        </div>
                        <div class="popular-courses-content">
                            <div class="author-review fix">
                                <ul class="pull-left">
                                    <li>By: <a href="#">Jhon dow</a></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><span>Review: </span></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-o"></i></li>
                                </ul>
                            </div>
                            <div class="popular-course-info">
                                <h3><a href="#">Code Create - Mandatory</a></h3>
                                <p>The branch of science concerned with the nature and properties of matter and energy. The subject matter of physics, distinguished from that of chemistry and biology.</p>
                            </div>
                            <div class="course-footer fix">
                                <ul class="pull-left">
                                    <li><span>Courses: <strong>1 year</strong></span></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><a href="#"><i class="fa fa-thumbs-o-up"></i></a></li>
                                    <li><a href="#"><i class="fa fa-commenting-o"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="popular-courses-wrap">
                        <div class="popular-courses-img">
                            <img src="assets/images/service/7.jpg" alt="">
                        </div>
                        <div class="popular-courses-content">
                            <div class="author-review fix">
                                <ul class="pull-left">
                                    <li>By: <a href="#">Jhon dow</a></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><span>Review: </span></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-o"></i></li>
                                </ul>
                            </div>
                            <div class="popular-course-info">
                                <h3><a href="#">laboratory - mandatory laboratory</a></h3>
                                <p>A medical laboratory or clinical laboratory is a boratorys pertaining to the diagnosis, treatment, and prevention of disease. information about the  game is not over.</p>
                            </div>
                            <div class="course-footer fix">
                                <ul class="pull-left">
                                    <li><span>Courses: <strong>1 year</strong></span></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><a href="#"><i class="fa fa-thumbs-o-up"></i></a></li>
                                    <li><a href="#"><i class="fa fa-commenting-o"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="popular-courses-wrap">
                        <div class="popular-courses-img">
                            <img src="assets/images/service/8.jpg" alt="">
                        </div>
                        <div class="popular-courses-content">
                            <div class="author-review fix">
                                <ul class="pull-left">
                                    <li>By: <a href="#">Jhon dow</a></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><span>Review: </span></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                            <div class="popular-course-info">
                                <h3><a href="#">physics - physics game for learners</a></h3>
                                <p>branch of science concerned with the nature and properties of matter and energy. The subject matter of physics, distinguished from that of chemistry and biology matter.</p>
                            </div>
                            <div class="course-footer fix">
                                <ul class="pull-left">
                                    <li><span>Courses: <strong>1 year</strong></span></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><a href="#"><i class="fa fa-thumbs-o-up"></i></a></li>
                                    <li><a href="#"><i class="fa fa-commenting-o"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col col-xs-12">
                    <div class="popular-courses-wrap">
                        <div class="popular-courses-img">
                            <img src="assets/images/service/9.jpg" alt="">
                        </div>
                        <div class="popular-courses-content">
                            <div class="author-review fix">
                                <ul class="pull-left">
                                    <li>By: <a href="#">Jhon dow</a></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><span>Review: </span></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-o"></i></li>
                                </ul>
                            </div>
                            <div class="popular-course-info">
                                <h3><a href="#">Code Create - Mandatory</a></h3>
                                <p>The branch of science concerned with the nature and properties of matter and energy. The subject matter of physics, distinguished from that of chemistry and biology.</p>
                            </div>
                            <div class="course-footer fix">
                                <ul class="pull-left">
                                    <li><span>Courses: <strong>1 year</strong></span></li>
                                </ul>
                                <ul class="pull-right">
                                    <li><a href="#"><i class="fa fa-thumbs-o-up"></i></a></li>
                                    <li><a href="#"><i class="fa fa-commenting-o"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="pagination-wrap text-center">
                        <ul>
                            <li><a href="#"><i class="fa fa-long-arrow-left"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#"><i class="fa fa-long-arrow-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- popular-courses-area end -->

@endsection

