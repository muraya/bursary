<?php

namespace App\Http\Controllers;

use App\Subscriptions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.subscriptions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $subscription = new Subscriptions();

        $subscription->name = $request->input('name');
        $subscription->email = $request->input('email');
        $subscription->status = 1; /// status for new user
        $subscription->created_at = Carbon::now();

        DB::table('mdr_notifications')->insert([
            [
                'title' => "New Subscription",
                'message' => "You have a new subscriber with mail ".$request->input('email'),
                'status' => 1,
                'created_at' => Carbon::now()
            ]
        ]);

        if ($subscription->save())
        {
            return back()->with('success','Thank you for subscribing');

        }
        else{
            return back()->with('error','We have encountered a problem processing youre request.Please Resend your mail');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $subscription = Subscriptions::findOrFail($id);
        $input = $request->all();
        $subscription->fill($input)->save();

        return response()->json(['status' => '00', 'message' => 'Subscription Details changes have been updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $subscription = Subscriptions::findOrFail($id);
        $subscription->delete();

        return response()->json(['status' => '00', 'message' => 'User Subscription has been Deleted Successfully']);
    }


    public function activeSubscriptions()
    {

        $actions ='

                <a href="#" type="button" class="btn btn-primary btn-xs active" data-toggle="modal" data-id="{{ $id }}"  
                data-name="{{$name}}" data-target="#activeModal" > <i class="fa fa-thumbs-up"></i></a>

                <a href="#" type="button" class="btn btn-danger btn-xs deactivate"  data-toggle="modal" data-id="{{ $id }}" 
                 data-name="{{$name}}" data-target="#deactivateModal"> <i class="fa fa-thumbs-down"></i> </a>
                 
                 ';

        $users = DB::table('mdr_subscriptions')
            ->get();

        return Datatables::of($users)
            ->addColumn('actions', $actions)
            ->rawColumns(['actions', 'actions'])
            ->make(true);

    }

}
