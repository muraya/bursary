<?php

namespace App\Http\Controllers;

use App\Traits\FormatAjaxValidationMessages;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    use FormatAjaxValidationMessages;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $roles = DB::table('mdr_roles')->get();
        return view('admin.users.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:mdr_users,email|max:255',
            'phone' => 'required|unique:mdr_users,phone|numeric',
        ]);
        //
        if($validator -> passes())
        {
            $User = new User();
            $role = $request->input('role');
            if($role == 1)
                $User->is_admin = 1;
            else
                $User->is_admin = 0;

            $User->name = $request->input('name');
            $User->sname = $request->input('sname');
            $User->email = $request->input('email');
            $User->phone = $request->input('phone');
            $User->role_id = $request->input('role');
            $User->password = Hash::make('secret');
            $User->status = 1; /// status for new user
            $User->created_at = Carbon::now();

            if ($User->save())
            {
                return response()->json(['status' => '00', 'message' => 'Created User Successfully']);

            }
            else{
                return response()->json(['status' => '01', 'message' => 'Error when creating user']);
            }
        }
        else{

            return response()->json(['status' => '01', 'message' => $this -> returnMessageString($validator->errors()->getMessages())]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::findOrFail($id);

        return view('users.show')->with('user',$user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);

        $response = array(
            "status" => "00",
            "id" => $user['id'],
            "name" => $user['name'],
            "sname" => $user['sname'],
            "email" => $user['email'],
            "phone" => $user['phone'],
            "role" => $user['role_id'],
        );

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
        ]);

        //
        if($validator -> passes()) {

            $user = User::findOrFail($id);
            $input = $request->all();
            $user->fill($input)->save();

            return response()->json(['status' => '00', 'message' => 'User Details changes have been updated Successfully']);
        }
        else{

            return response()->json(['status' => '01', 'message' => $this -> returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json(['status' => '00', 'message' => 'User has been Deleted Successfully']);
    }

    public function activeUsers()
    {

        $users = DB::table('mdr_users')
            ->join('mdr_roles', 'mdr_users.role_id', '=', 'mdr_roles.id')
            ->select('mdr_users.*', 'mdr_roles.name as role_name', 'mdr_users.status as user_status')
            ->get();

        return Datatables::of($users)
            ->editColumn('user_status', function ($user) {
                if ($user->user_status == 1) return '

                <small class="label pull-right bg-green"> Active </small>
                 
                ';
                if ($user->user_status != 1)
                    return '

                <small class="label pull-right bg-yellow"> Inactive</small>
                 
                 ';
                return '';
            })
            ->editColumn('actions', function ($user) {
                if ($user->user_status == 1) return '

                <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-id="'.$user->id .'"  
                data-name="'.$user->name .'" data-target="#editModal" > <i class="fa fa-edit"></i></a>
                
                <a href="#" type="button" class="btn btn-warning btn-xs deactivate" data-toggle="modal" data-id="'.$user->id .'"  
                data-name="'.$user->name .'" title="Deactivate User" data-target="#deactivateModal" > <i class="fa fa-thumbs-down"></i></a>  
                              
                <a href="#" type="button" class="btn btn-danger btn-xs delete"  data-toggle="modal" data-id="'.$user->id .'" 
                 data-name="'.$user->name .'" data-target="#deleteModal"> <i class="fa fa-trash-o"></i> </a>
                 
                ';
                if ($user->user_status != 1)
                    return '

                <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-id="'.$user->id .'"  
                 data-name="'.$user->name .'" data-target="#editModal" > <i class="fa fa-edit"></i></a>  
                                 
                <a href="#" type="button" class="btn btn-success btn-xs activate" data-toggle="modal" data-id="'.$user->id .'"  
                data-name="'.$user->name .'" title="Activate User" data-target="#activateModal" > <i class="fa fa-thumbs-up"></i></a>
                
                <a href="#" type="button" class="btn btn-danger btn-xs delete"  data-toggle="modal" data-id="'.$user->id .'" 
                 data-name="'.$user->name .'" data-target="#deleteModal"> <i class="fa fa-trash-o"></i> </a>
                 
                 ';
                return '';
            })
            ->rawColumns(['user_status','actions'])
            ->make(true);

    }


    public function activate(Request $request){

        $user_id = $request ->input('id');

        DB::table('mdr_users')
            ->where('id', $user_id)
            ->update(['status' => 1]);

        return response()->json(['status' => '00', 'message' => 'User has been Activated Successfully']);

    }

    public function deactivate(Request $request){

        $user_id = $request ->input('id');

        $user_details = User::find($user_id);

        if($user_id != Auth::id()) {
            DB::table('mdr_users')
                ->where('id', $user_id)
                ->update(['status' => 2]);

            return response()->json(['status' => '00', 'message' => 'User has been Deactivated Successfully']);
        }
        elseif($user_details -> is_admin != 1){
            return response()->json(['status' => '01', 'message' => 'You cant deactivate an admin account']);
        }
        else {
            return response()->json(['status' => '01', 'message' => 'Error when creating user']);
        }
    }

    public function profile()
    {
        return view('admin.users.profile');
    }

    public function postProfile(Request $request)
    {

    }

}
