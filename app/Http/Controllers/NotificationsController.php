<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.notifications.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function activeNotifications()
    {

        $actions ='
                <a href="#" type="button" class="btn btn-primary btn-xs seen" data-id="{{ $id }}"  
                data-name="{{$title}}"> <i class="fa fa-eye"></i></a>

                <a href="#" type="button" class="btn btn-danger btn-xs delete"  data-toggle="modal" data-id="{{ $id }}" 
                 data-name="{{$title}}" data-target="#deleteModal"> <i class="fa fa-trash-o"></i> </a>
                 
                 ';

        $users = DB::table('mdr_notifications')
            ->get();

        return Datatables::of($users)
            ->addColumn('actions', $actions)
            ->rawColumns(['actions', 'actions'])
            ->make(true);

    }

}
