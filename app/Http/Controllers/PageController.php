<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contacts;
use App\Products;
use Carbon\Carbon;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('index');
    }

    public function about() // index about bursary organisation contact
    {
        return view('about');

    }

    public function bursary()
    {
        return view('bursary');
    }

    public function organisation()
    {
        return view('bursary');
    }

    public function contact()
    {
        return view('contact');
    }

    public function postContacts(Request $request)
    {
        $Contact = new Contacts();

        $Contact->name = $request->input('name');
        $Contact->email = $request->input('email');
        $Contact->phone = $request->input('phone');
        $Contact->message = $request->input('message');
        $Contact->created_at = Carbon::now();

        if ($Contact->save())
        {
            return back()->with('success','Your mail has been sent Successfully');
        }
        else
        {
            return back()->with('error','We have encountered an Error handling youren request . Please try again');
        }
    }



}
