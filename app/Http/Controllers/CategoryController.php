<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Traits\FormatAjaxValidationMessages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;

class CategoryController extends Controller
{
    use FormatAjaxValidationMessages;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        //
        if($validator -> passes())
        {
            $categories = new Categories();

            $categories->name = $request->input('name');
            $categories->description = $request->input('description');
            $categories->status = 1;
            $categories->created_at = Carbon::now();

            if ($categories->save())
            {
                return response()->json(['status' => '00', 'message' => 'Created Category Successfully']);

            }
            else{
                return response()->json(['status' => '01', 'message' => 'Error when creating Category']);
            }
        }

        else{

            return response()->json(['status' => '01', 'message' => $this -> returnMessageString($validator->errors()->getMessages())]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Categories::find($id);

        $response = array(
            "status" => "00",
            "id" => $category['id'],
            "name" => $category['name'],
            "description" => $category['description'],
        );

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $category = Categories::findOrFail($id);
        $input = $request->all();
        $category->fill($input)->save();

        return response()->json(['status' => '00', 'message' => 'Category Details changes have been updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $categoryProducts = DB::table('mdr_products')
            ->where('category_id',$id)
            ->get();

//        dd($categoryProducts);
        if ($categoryProducts->isEmpty())
        {
            $category = Categories::findOrFail($id);
            $category->delete();

            return response()->json(['status' => '00', 'message' => 'Category has been Deleted Successfully']);
        }
        else{
            return response()->json(['status' => '01', 'message' => 'Category has an associated product -- Cannot be deleted']);
        }

    }

    public function activecategories()
    {

        $actions ='

                <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-id="{{ $id }}"  
                data-name="{{$name}}" data-target="#editModal" > <i class="fa fa-edit"></i></a>
                
                <a href="#" type="button" class="btn btn-danger btn-xs delete" data-toggle="modal" data-id="{{ $id }}"  
                data-name="{{$name}}" data-target="#deleteModal" > <i class="fa fa-trash"></i></a>
                 ';

        $roles = Categories::query();

        return Datatables::of($roles)
            ->addColumn('actions', $actions)
            ->rawColumns(['actions', 'actions'])
            ->make(true);

    }

}
