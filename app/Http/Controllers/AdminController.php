<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.index');
    }

    public function users()
    {
        return view('admin.users.index');
    }

    public function applicants()
    {
        return view('admin.applicants.index');
    }

    public function categories()
    {
        return view('admin.categories.index');
    }

    public function applications()
    {
        return view('admin.applications.index');
    }

    public function disbursements()
    {
        return view('admin.disbursements.index');
    }

    public function subscriptions()
    {
        return view('admin.subscriptions.index');
    }

    public function clients()
    {
        return view('admin.clients.index');
    }

    public function contacts()
    {
        return view('admin.contacts.index');
    }

    public function profile()
    {
        $user = Auth::user();
        return view('admin.users.profile',compact('user'));
    }

    public function postProfile(Request $request)
    {



        $User = User::find(Auth::id());

        $User->name = $request->input('name');
        $User->email = $request->input('email');
        $User->phone = $request->input('phone');
        $User->created_at = Carbon::now();

        if(!empty($request -> file('file')))
        {
            $image = $request->file('file');
            $input['image_name'] = time().'.'.$image->getClientOriginalExtension();
            $image_path = 'uploads/avatars';
            $image_img = Image::make($image->getRealPath());
            $image_img->resize(76, 76)->save($image_path.'/'.$input['image_name']);

            $User->avatar = $image_path.'/'.$input['image_name'];
        }

        $User->save();

        return back()->with('success','Profile updated successfully!');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function password()
    {
        return view('admin.users.password');
    }

    public function postPassword(Request $request)
    {
        $current = $request->input('current_password');
        $new = $request->input('new_password');
        $confirm = $request->input('confirm_password');
        if(Hash::check($current, Auth::user()->password))
        {
            if($new == $confirm)
            {
                User::where('id', Auth::id())
                    ->update(['password' => Hash::make($new)]);

                return back()->with('success','Password updates successfully!');
            }
            else
            {
                return back()->with('error','Your Confirmation password does not match');
            }
        }
        else
        {
            return back()->with('error','Your current password does not match');
        }
    }
}
