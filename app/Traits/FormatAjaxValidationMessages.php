<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/12/2017
 * Time: 5:53 PM
 */

namespace App\Traits;


trait FormatAjaxValidationMessages
{
    public function returnMessageString($messages){

        $errors = '';
        foreach($messages as $validationErrors)
        {
            if(is_array($validationErrors)){
                foreach ($validationErrors as $key => $validationError)
                {
                    $errors .= $validationError."</br>";
                }
            }
        }

        return $errors;

    }
}