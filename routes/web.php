<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');
Route::get('about', 'PageController@about');
Route::get('bursary', 'PageController@bursary');
Route::get('organisation', 'PageController@organisation');
Route::get('contact', 'PageController@contact');
Route::post('contacts', 'PageController@postContacts');
Route::resource('sitemap', 'SitemapController');

Auth::routes();


Route::get('/admin', 'AdminController@index');
Route::get('/home', 'AdminController@index');

Route::get('admin/users', 'AdminController@users');
Route::get('admin/applicants', 'AdminController@applicants');

Route::get('admin/categories', 'AdminController@categories');
Route::get('admin/bursaries', 'AdminController@bursaries');
Route::get('admin/applications', 'AdminController@applications');
Route::get('admin/disbursements', 'AdminController@disbursements');

Route::get('admin/subscriptions', 'AdminController@subscriptions');
Route::get('admin/contacts', 'AdminController@contacts');

Route::get('admin/settings', 'AdminController@settings');
Route::get('/admin/profile', 'AdminController@profile');
Route::get('admin/password', 'AdminController@password');
Route::post('admin/password', 'AdminController@postPassword');
Route::post('admin/profile', 'AdminController@postProfile');

Route::get('usersdata', 'UserController@usersData');
Route::post('users/activate', 'UserController@activate');
Route::post('users/deactivate', 'UserController@deactivate');
Route::resource('users', 'UserController');

Route::get('students', 'StudentsController@usersData');
Route::post('students/activate', 'StudentsController@activate');
Route::post('students/deactivate', 'StudentsController@deactivate');
Route::resource('students', 'StudentsController');

Route::get('rolesdata', 'RoleController@rolesData');
Route::resource('roles','RoleController');

Route::get('categoriesdata', 'CategoryController@categoriesData');
Route::post('categories/activate', 'CategoryController@activate');
Route::post('categories/deactivate', 'CategoryController@deactivate');
Route::resource('categories','CategoryController');

Route::get('productcategoriesdata', 'ProductcategoriesController@projectCategoriesData');
Route::post('productcategories/activate', 'ProductcategoriesController@activate');
Route::post('productcategories/deactivate', 'ProductcategoriesController@deactivate');
Route::resource('productcategories','ProductcategoriesController');

Route::get('productsdata', 'ProductController@productsData');
Route::post('products/sale', 'ProductController@forSale');
Route::post('products/hire', 'ProductController@forHire');
Route::post('products/activate', 'ProductController@activate');
Route::post('products/deactivate', 'ProductController@deactivate');
Route::resource('products','ProductController');


Route::get('servicesdata', 'ServicesController@servicesData');
Route::post('service/activate', 'ServicesController@activate');
Route::post('service/deactivate', 'ServicesController@deactivate');
Route::resource('service','ServicesController');

Route::get('projectsdata', 'ProjectsController@projectsData');
Route::post('project/activate', 'ProjectsController@activate');
Route::post('project/deactivate', 'ProjectsController@deactivate');
Route::resource('project','ProjectsController');

Route::get('subscriptionsdata', 'SubscriptionController@subscriptionsData');
Route::resource('subscriptions','SubscriptionController');

Route::get('contactsdata', 'ContactsController@contactsData');
Route::resource('contacts','ContactsController');

Route::resource('configurations', 'ConfigurationController');

Route::get('activenotifications', 'NotificationsController@activeNotifications');
Route::resource('notifications', 'NotificationsController');


Route::get('emails', function () {
    return view ('emails.info');
});
